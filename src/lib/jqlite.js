import JQObject from './JQObject.js';

export default function $(querySelector, element = document) {
	if (typeof querySelector === 'object') {
		if (querySelector instanceof JQObject) {
			return querySelector;
		}
		querySelector = querySelector.className
			.split(' ')
			.map(className => '.' + className)
			.join(' ');
	}
	return new JQObject(element.querySelectorAll(querySelector));
}
