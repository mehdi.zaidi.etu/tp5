import $ from './jqlite.js';

export default class JQObject {
	constructor(DOMElements) {
		let i = 0;
		DOMElements.forEach(Element => {
			this[i] = Element;
			i++;
		});
		this.length = i;
	}

	html(newHTML) {
		for (let i = 0; i < this.length; i++) {
			this[i].innerHTML = newHTML;
		}
		return this;
	}

	append(newHTML) {
		return this.html(this.html() + newHTML);
	}

	find(querySelector) {
		return $(querySelector, this[0]);
	}

	addClass(className) {
		for (let i = 0; i < this.length; i++) {
			this[i].classList.add(className);
		}
		return this;
	}

	removeClass(className) {
		for (let i = 0; i < this.length; i++) {
			this[i].classList.remove(className);
		}
		return this;
	}

	attr(attributeName, newValue = null) {
		if (newValue != null) {
			for (let i = 0; i < this.length; i++) {
				this[i].setAttribute(attributeName, newValue);
			}
			return this;
		}

		return this[0].getAttribute(attributeName);
	}

	removeAttr(attributeName) {
		for (let i = 0; i < this.length; i++) {
			this[i].removeAttribute(attributeName);
		}
		return this;
	}

	val(newValue = null) {
		if (newValue) {
			for (let i = 0; i < this.length; i++) {
				this[i].value = newValue;
			}
			return this;
		}
		return this[0].value;
	}

	on(eventName, eventHandler) {
		if (this[0]) {
			this[0].addEventListener(eventName, eventHandler);
		}
		return this;
	}
}
