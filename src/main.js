import AboutView from './AboutView.js';
import GameDetailView from './GameDetailView.js';
import GameListView from './GameListView.js';
import HelpView from './HelpView.js';
import Router from './Router.js';
import $ from './lib/jqlite.js';

$('.logo').on('click', event => {
	event.preventDefault();
	Router.navigate('/', true);
});

$('body > footer a').on('mouseover', event => {
	$(event.currentTarget).addClass('mouseover');
});

$('body > footer a').on('mouseout', event => {
	$(event.currentTarget).removeClass('mouseover');
});

// Modification du footer
$('body > footer > div:nth-of-type(2)').append(
	' / CSS inspirée de <a href="https://store.steampowered.com/">steam</a>'
);

// création des vues de notre application
const helpView = new HelpView($('.viewContent .help'));
const gameListView = new GameListView($('.viewContent > .gameList'));
const aboutView = new AboutView($('.viewContent > .about'));
const gameDetailView = new GameDetailView($('.viewContent > .gameDetail'));

// mise en place du Router
const routes = [
	{ path: '/', view: gameListView, title: 'Magasin' },
	{ path: '/about', view: aboutView, title: 'À propos' },
	{ path: '/help', view: helpView, title: 'Support' },
	{ path: '/detail-*', view: gameDetailView, title: 'Détail jeu' },
];
Router.routes = routes;
// élément dans lequel afficher le <h1> de la vue
Router.titleElement = $('.viewTitle');
// gestion des liens du menu (détection du clic et activation/désactivation)
Router.setMenuElement($('.mainMenu'));

// chargement de la vue initiale selon l'URL demandée par l'utilisateur.rice (Deep linking)
Router.navigate(window.location.pathname, true);
// gestion des boutons précédent/suivant du navigateur (History API)
window.onpopstate = () => Router.navigate(document.location.pathname, true);

// console.log($('.logo span').html('<em>jquery</em>steam'));
// console.log($('a').html('<em>jquery</em>steam'));

// import $$ from './lib/jqlite.js';

// $('.testdiv').find('.test').attr('test', 'hello');
// $$('.testdiv2').find('.test2').attr('test', 'hello');
